package com.example.testpatch.controller;


import com.example.testpatch.dto.Order;
import com.example.testpatch.dto.Product;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;

@RestController
public class TestPatchController {

    private ObjectMapper objectMapper;


    public TestPatchController(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @PatchMapping(value = "/{id}", consumes = "application/json-patch+json")
    public ResponseEntity<Order> updateOrder(@PathVariable Integer id, @RequestBody JsonNode jsonNode) throws IOException {

        Order dbOrder = getOrder(id);

        ObjectReader readerForUpdating = objectMapper.readerForUpdating(dbOrder);

        Order orderPatched = readerForUpdating.readValue(jsonNode);
        return ResponseEntity.ok(orderPatched);
    }


    private Order getOrder(int id) throws JsonProcessingException {


        Order result = new Order();
        result.setClient("Client1");
        result.setId(1);
        result.setProducts(new ArrayList<>());
        result.getProducts().add(new Product("nom1", "description1"));
        System.out.println(objectMapper.writeValueAsString(result));
        return result;


    }


}
