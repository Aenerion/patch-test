package com.example.testpatch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestPatchApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestPatchApplication.class, args);
    }

}
